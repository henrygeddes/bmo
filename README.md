# BMO 

BMO (pronounced Beemo) is a Voice Engine for text-to-speech.
BMO uses [Merlin](https://github.com/CSTR-Edinburgh/merlin).


# Requirements

Python 3.6
[pyenv](https://github.com/pyenv/pyenv)


# Setup
`pip install virtualenv`
`source env/bin/activate`
`pip install -Iv numpy==1.18.2`
`cd libs/merlin`
` pip install -r requirements.txt`


# Usage
Coming soon


# Contact Us
Please don't
